AWS RDS Mysql
================
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create RDS Mysql services with CloudFormation.

AWS resources that will be created are:
 * RDS MySQL instance
 * SecurityGroup

Requirements
------------
Ansible version 2.5.4 or higher
Python 2.7.x

Required python modules:
 * boto
 * boto3
 * awscli

Dependencies
------------
 * aws-vpc-lambda-cfn-dbprovider
 * aws-iam
 * aws-vpc

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
-------------
```yaml
---
create_changeset      : True
debug                 : False
cloudformation_tags   : {}
tag_prefix            : "mcf"

aws_rds_mysql:
  stack_name      : "{{ slice_name | default(environment_abbr) }}-{{ aws_rds_mysql_params.rds.name }}-data"

  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"

  database:
    name         : "{{ aws_rds_mysql.rds.name }}" # Name of the db scheme created by RDS
    username     : "{{ aws_rds_mysql.rds.name }}" # DB scheme user, password will be auto generated and saved in SSM

  rds:
    name         : # RDS instance name
    username     : "root"
    storage      : 50
    instance_type: "db.t2.small"
    snapshot_name: "" # Optional
    version      : "5.7"
    multi_az     : False
    encrypt      : True
```

Example Playbooks
-----------------
Rollout the aws-rds-mysql files with defaults
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53:
      domainname: my.cloud
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
    aws_lambda_params:
      lambda_roles:
        - aws-lambda-cfn-secretsprovider
  roles:
    - aws-setup
    - aws-iam
    - aws-vpc
    - env-acl
    - aws-vpc-lambda
    - aws-securitygroups
    - aws-lambda
    - aws-rds-mysql
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
